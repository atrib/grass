#define CATCH_CONFIG_MAIN
#include "inc/catch.hpp"

#include "inc/parser/command.hpp"
#include "inc/parser/parser.hpp"

using namespace std;

void split_snip(string stream, vector<string> &snippets) {
  auto len = stream.length();

  srand(time(NULL));
  do{
    auto pivot = (rand() % len) + 1;

    snippets.push_back(stream.substr(0, pivot));
    stream = stream.substr(pivot, string::npos);
    len -= pivot;
  } while (len > 0);
}

TEST_CASE( "CommandParser", "[commands]" ) {
  CommandParser parser;

  string stream = "ping\ndate\n   ping \n ping\n   date\ndate   \n";
  vector<string> expected_cmds = {"ping", "date", "ping", "ping", "date", "date"};
  
  vector<string> snippets;
  split_snip(stream, snippets);

  int count = 0;
  for(auto snippet : snippets){
    cout << "Adding bytes: " << snippet << endl;
    if(parser.addBytes(snippet)) {
      do {
        cout << "New command found " << endl;
        /* Check count-th command */
        auto *cmd = parser.getCommand();
        REQUIRE(cmd->cmd == expected_cmds[count]);
        REQUIRE(cmd->args.size() == 0);

        delete cmd;
        ++count;
      } while(parser.hasCommand());
    }
    else
        cout << "No new command" << endl;

  }
  REQUIRE(count == expected_cmds.size());
}