SRCDIR   := src
BINDIR   := bin
INCLUDES := inc

CC=gcc
CPP=g++
CFLAGS=-Wall -Wextra -g -fno-stack-protector -m32 -z execstack -pthread -I . -Wl,--section-start=.text=0x11111111


#CLIENT_INC = $(INCLUDES)/client/client.hpp
CLIENT_SRC = $(SRCDIR)/client/client.cpp
#CLIENT = $(CLIENT_INC) $(CLIENT_SRC)

#COMMANDS_INC = $(INCLUDES)/commands
COMMANDS_SRC = $(SRCDIR)/commands/command.cpp $(SRCDIR)/commands/parser.cpp
#COMMANDS = $(CLIENT_INC) $(CLIENT_SRC)

#SERVICES_INC = $(INCLUDES)/services
SERVICES_SRC = $(SRCDIR)/services/logging.cpp $(SRCDIR)/services/socket.cpp $(SRCDIR)/services/threadpool.cpp
#SERVICES = $(CLIENT_INC) $(CLIENT_SRC)

#USER_INC = $(INCLUDES)/user/user.hpp
USER_SRC = $(SRCDIR)/user/user.cpp
#USER = $(CLIENT_INC) $(CLIENT_SRC)

#GRASS_INC = $(INCLUDES)/grass.hpp
GRASS_SRC = $(SRCDIR)/grass.c
#GRASS = $(CLIENT_INC) $(CLIENT_SRC)

SERVER_SRC = $(SRCDIR)/server/server.cpp


all: directories $(BINDIR)/client $(BINDIR)/server

directories: ${BINDIR}

${BINDIR}:
	mkdir -p ${BINDIR}; mkdir -p ${BINDIR}/basedir

$(BINDIR)/client: $(SRCDIR)/client/client_main.cpp $(SERVICES_SRC) $(CLIENT_SRC) $(GRASS_SRC)
	$(CPP) $(CFLAGS) $^ -o $@

$(BINDIR)/server: $(SRCDIR)/server/server_main.cpp $(COMMANDS_SRC) $(SERVER_SRC) $(SERVICES_SRC) $(USER_SRC) $(GRASS_SRC)
	$(CPP) $(CFLAGS) $^ -o $@

.PHONY: clean
clean:
	rm -f -r $(BINDIR)/client $(BINDIR)/server
