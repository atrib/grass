# GRASS is buggier on the other side

## Students
* [Atri Bhattacharyya](mailto:atri.bhattacharyya@epfl.ch)
* [Stevan Ognjanovic](mailto:stevan.ognjanovic@epfl.ch)

## Project structure before compilation

```
softsecepfl
|   .gitignore
|   makefile
|   README.md
|   README.pdf
|   grass.conf
----inc
|   |   catch.hpp
|   |   grass.h
|   ----client
|   |   |   client.hpp
|   |   
|   ----commands
|   |   |   command.hpp
|   |   |   parser.hpp
|   |   |   util.hpp
|   |
|   ----server
|   |   |   server.hpp
|   |
|   ----services
|   |   |   logging.hpp
|   |   |   socket.hpp
|   |   |   threadpool.hpp
|   |
|   ----user
|   |   |   user.hpp
----src
|   |   grass.c
|   ----client
|   |   |   client.cpp
|   |   |   client_main.cpp
|   |   
|   ----commands
|   |   |   command.cpp
|   |   |   parser.cpp
|   |
|   ----server
|   |   |   server.hpp
|   |   |   server_main.cpp
|   |
|   ----services
|   |   |   logging.cpp
|   |   |   socket.cpp
|   |   |   threadpool.cpp
|   |
|   ----user
|   |   |   user.cpp
|
----exploits (made exploits should be extracted here for scripts to work)
----tests
    ....
```

## Compile and run
To compile the project run `make` in the root directory of the project, it will create two directories one for client
and one for server. For running the server and the client with the provided server configuration execute the following
commands.

#### Server
```
cd bin
./server
```
#### Client
```
cd bin
./client 127.0.0.1 1337
```

## Explanations
#### Client
Client specific implementation is contained inside the inc/client and src/client.
#### Commands and parsers
Implementation of client commands, command parsing and config file parsing is contained inside
the inc/commands and src/commands. For each command we have a specific class for each command, 
to make the implementation easier to reason about we have a parser which based on the user input 
constructs a command ready for the server to execute. 

For server configuring we have a parser which reads the config file and extracts the necessary information
from it.
#### Server
Server implementation can be found in inc/server and src/server. Server can execute multiple file
transfers at the same time.
#### Services
Inside inc/service and src/services contain classes which we used to decompose our code. We have 
extracted a socket wrapper, logging service and a thread pool used to run control and data thread.
#### User
Each user has his own session object, the implementation is contained in inc/user and src/user.

## Exploits password
`xQDq9iyFKnFL75/jvSzm2rxhvtw9Pla1lLfxoVdsFo8=`